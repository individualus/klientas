import Comment from './Comment.vue';
import Comments from './Comments.vue';

export {
    Comment,
    Comments
};