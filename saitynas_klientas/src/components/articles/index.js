import Article from './Article.vue';
import Articles from './Articles.vue';

export {
    Article,
    Articles
};