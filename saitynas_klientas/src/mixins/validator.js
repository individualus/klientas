export default {
    methods: {
        validateRequiredField(field) {
            return value => !!value || `${field} is required`;
        }
    }
};