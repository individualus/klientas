export default {
    methods: {
        handleError(err) {
            this.$root.$emit(
                'showNotification',
                {
                    message: (
                        (
                            err.resposne && (
                                (
                                    err.resposne.data &&
                                    err.response.data.Message
                                ) ||
                                `An error occured on the server: ${err.resposne.statusText}`
                            )
                        ) ||
                        err.message ||
                        'An errror occured.'
                    ),
                    type: 'error'
                }
            );
        }
    }
};