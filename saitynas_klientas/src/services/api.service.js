import store from '@/store';
import axios from 'axios';

function request(config) {
    return axios.request({
        baseURL: process.env.VUE_APP_SERVER_API_URL,
        headers: {
            Authorization: `Bearer ${store.getters['auth/oidcAccessToken']}`
        },
        ...config
    });
}

export default {
    request
};