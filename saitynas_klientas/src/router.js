import { Article, Articles } from '@/components/articles';
import { Comments } from '@/components/comments';
import Home from '@/components/Home.vue';
import { LoginCallback, LogoutCallback, SilentRenewCallback } from '@/components/auth';
import { User, Users } from '@/components/users';

import { roles } from '@/config';
import Router from 'vue-router';
import store from '@/store';
import Vue from 'vue';
import { vuexOidcCreateRouterMiddleware } from 'vuex-oidc';

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/article/:id',
            name: 'Article',
            component: Article,
            meta: {
                isPublic: true,
                title: 'Article'
            }
        },
        {
            path: '/articles',
            name: 'Articles',
            component: Articles,
            meta: {
                isPublic: true,
                title: 'Articles'
            }
        },
        {
            path: '/comments',
            name: 'Comments',
            component: Comments,
            meta: {
                isPublic: false,
                role: roles.Administrator,
                title: 'Comments'
            }
        },
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: {
                isPublic: true,
                title: 'Home'
            }
        },
        {
            path: '/oidc-login',
            name: 'LoginCallback',
            component: LoginCallback,
            meta: {
                isPublic: true
            }
        },
        {
            path: '/oidc-logout',
            name: 'LogoutCallback',
            component: LogoutCallback,
            meta: {
                isPublic: true
            }
        },
        {
            path: '/oidc-silent-renew',
            name: 'SilentRenewCallback',
            component: SilentRenewCallback,
            meta: {
                isPublic: true
            }
        },
        {
            path: '/user/:id',
            name: 'User',
            component: User,
            beforeEnter: (to, from, next) => {
                if (store.getters['user/role'] >= roles.Administrator || to.params.id === store.getters['user/id']) {
                    next()
                } else {
                    next(false);
                }
            },
            meta: {
                isPublic: false,
                title: 'User'
            }
        },
        {
            path: '/users',
            name: 'Users',
            component: Users,
            meta: {
                isPublic: false,
                role: roles.Administrator,
                title: 'Users'
            }
        },
        {
            path: '*',
            redirect: '/'
        }
    ]
});

router.beforeEach(function (to, from, next) {
    if (!to.meta.isPublic && (to.meta.role || roles.Anonymous) > store.getters['user/role']) {
        next(false);
    } else {
        vuexOidcCreateRouterMiddleware(store, 'auth')(...arguments);
    }
});

router.afterEach((to) => {
    document.title = to.meta.title || process.env.VUE_APP_DEFAULT_TITLE;
});

export default router;
