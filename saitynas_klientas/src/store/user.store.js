export default {
    namespaced: true,
    state: () => ({
        email: '',
        firstName: '',
        id: 0,
        lastName: '',
        role: 0,
        username: ''
    }),
    getters: {
        email: state => state.email,
        firstName: state => state.firstName,
        id: state => state.id,
        lastName: state => state.lastName,
        role: state => state.role,
        username: state => state.username
    },
    mutations: {
        email: (state, email) => state.email = email || '',
        firstName: (state, firstName) => state.firstName = firstName || '',
        id: (state, id) => state.id = id || 0,
        lastName: (state, lastName) => state.lastName = lastName || '',
        role: (state, role) => state.role = role || 0,
        username: (state, username) => state.username = username || ''
    },
    actions: {
        clear({ commit }) {
            commit('email');
            commit('firstName');
            commit('id');
            commit('lastName');
            commit('role');
            commit('username');
        },
        set({ commit, state }, user) {
            commit('email', user.Email || state.email);
            commit('firstName', user.FirstName || state.firstName);
            commit('id', user.Id || state.id);
            commit('lastName', user.LastName || state.lastName);
            commit('role', user.Role || state.role);
            commit('username', user.Username || state.username);
        }
    }
};