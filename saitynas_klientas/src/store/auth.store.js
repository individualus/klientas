import { auth } from '@/config';
import { vuexOidcCreateStoreModule } from 'vuex-oidc';

export default vuexOidcCreateStoreModule(
    auth,
    {
        isAuthenticatedBy: 'access_token',
        namespaced: true
    }
);