import auth from './auth.store';
import user from './user.store'

import createPersistedState from 'vuex-persistedstate';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,
    modules: {
        auth,
        user
    },
    plugins: [createPersistedState({
        paths: ['user']
    })]
});
