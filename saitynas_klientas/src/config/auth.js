export default {
    audience: 'server',
    authority: 'https://autorizacija.azurewebsites.net/',
    automaticSilentRenew: true,
    clientId: 'saitynas_client',
    postLogoutRedirectUri: `${process.env.VUE_APP_BASE_URL}oidc-logout/`,
    redirectUri: `${process.env.VUE_APP_BASE_URL}oidc-login/`,
    responseType: 'code',
    scope: 'openid server',
    silentRedirectUri: `${process.env.VUE_APP_BASE_URL}oidc-silent-renew/`
}
