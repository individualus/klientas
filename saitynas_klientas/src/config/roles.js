export default {
    Anonymous: 0,
    User: 100,
    Editor: 200,
    Administrator: 300
};